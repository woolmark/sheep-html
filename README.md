Sheep for HTML Canvas
----------------------------------------------------------------------------
Sheep for HTML Canvas is an implementation [sheep] for HTML.

Put your web site
----------------------------------------------------------------------------
``` html
<canvas id="woolmark_canvas" width="120" height="120"></canvas>
<script type="text/javascript" src="http://woolmark.bitbucket.org/badge/sheep.js" ></script>
```

License
----------------------------------------------------------------------------
[wtfpl] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[wtfpl]: http://www.wtfpl.net "WTFPL"
